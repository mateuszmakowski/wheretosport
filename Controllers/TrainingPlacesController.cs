﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WhereToSport.Models;

namespace WhereToSport.Controllers
{
    public class TrainingPlacesController : Controller
    {
        private readonly WhereToSportContext _context;

        public TrainingPlacesController(WhereToSportContext context)
        {
            _context = context;
        }

        // GET: TrainingPlaces
        public async Task<IActionResult> Index()
        {
            return View(await _context.TrainingPlace.ToListAsync());
        }

        // GET: TrainingPlaces/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainingPlace = await _context.TrainingPlace
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trainingPlace == null)
            {
                return NotFound();
            }

            return View(trainingPlace);
        }

        // GET: TrainingPlaces/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TrainingPlaces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] TrainingPlace trainingPlace)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trainingPlace);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trainingPlace);
        }

        // GET: TrainingPlaces/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainingPlace = await _context.TrainingPlace.FindAsync(id);
            if (trainingPlace == null)
            {
                return NotFound();
            }
            return View(trainingPlace);
        }

        // POST: TrainingPlaces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] TrainingPlace trainingPlace)
        {
            if (id != trainingPlace.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trainingPlace);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrainingPlaceExists(trainingPlace.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trainingPlace);
        }

        // GET: TrainingPlaces/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainingPlace = await _context.TrainingPlace
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trainingPlace == null)
            {
                return NotFound();
            }

            return View(trainingPlace);
        }

        // POST: TrainingPlaces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trainingPlace = await _context.TrainingPlace.FindAsync(id);
            _context.TrainingPlace.Remove(trainingPlace);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrainingPlaceExists(int id)
        {
            return _context.TrainingPlace.Any(e => e.Id == id);
        }
    }
}
