﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WhereToSport.Models
{
    public class WhereToSportContext : DbContext
    {
        public WhereToSportContext (DbContextOptions<WhereToSportContext> options)
            : base(options)
        {
        }

        public DbSet<WhereToSport.Models.TrainingPlace> TrainingPlace { get; set; }
    }
}
